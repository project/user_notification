<?php

namespace Drupal\user_notification\Services;

/**
 * Class NotificationUtil.
 */
class NotificationUtil {

  protected $entity;
  protected $opt;

  /**
   * Helper function to create notification.
   */
  public function createNotification() {
    $notification = [
      'title' => $this->entity->getEntity()->getTitle(),
      'entity_id' => $this->entity->getEntity()->id(),
      'entity_type' => $this->entity->getEntity()->bundle(),
      'status' => TRUE,
      'operation' => $this->opt,
    ];

    \Drupal::entityTypeManager()->getStorage('user_notification')->create($notification)->save();

  }

  /**
   * Helper function to update notification.
   */
  public function updateNotification() {

  }

  /**
   * Sets the entity instance for this mapper.
   *
   * @return $this
   */
  public function setEntity($event, $opt) {
    $this->opt = $opt;
    $this->entity = $event;
    return $this;
  }

}
